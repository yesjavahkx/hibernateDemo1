package com.test;

import com.hkx.hbn.demo.domain.Employee;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class HbnTest {
    private Session session;//最核心的接口，操作数据库的
    @Before
    public void before(){
        //获取Session 对象的步骤
//        1. 读取配置文件
        Configuration configuration = new Configuration().configure("com/hkx/hbn/demo/cfg/hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        session = sessionFactory.openSession();
        session.getTransaction().begin();
        //sessionFactory.getCurrentSession();

    }
    @After
    public void after(){
        session.getTransaction().commit();
    }

    /*
    保存一个对象的多种方式
     */
    @Test
    public void test1(){

        //session.save(employee);
        // session.saveOrUpdate(employee);
        for (int i = 0; i <100 ; i++) {
            Employee employee = new Employee().setFirstName("黄").setLastName("精猫").setSalary(6);
            session.persist(employee);

        }
    }



    /*
    根据主键查询对象
    get  不支持延迟加载，当主键不存在的时候返回nul，
    load 支持延迟加载，当主键不存在的时候抛异常，
   */
    @Test
    public void test2(){
//        Employee employee = session.get(Employee.class, 3);
        Employee employee = session.load(Employee.class, 30);
        System.out.println(employee);
    }

    /*
   根据主键 更新 对象
  */
    @Test
    public void test3(){
        Employee employee = session.load(Employee.class, 3);
        employee.setFirstName("李");
        session.update(employee);
    }

    /*
  根据主键 删除对象
 */
    @Test
    public void test4(){
        Employee employee = new Employee().setId(3);
        session.delete(employee);
    }

/*
    HQL 查询
*/

    @Test
    public void test5(){
      // String hql = "select new Employee(e.id,e.firstName)  from Employee e where e.salary > ? ";
        String hql = " from Employee e where e.id > ? ";
        Query query = session.createQuery(hql);
        query.setParameter(0,1);
        query.setFirstResult(5);
        query.setMaxResults(12);
        List<Employee> list = query.list();
        for (Employee employee : list) {
            System.out.println(employee);
        }
      // Employee o = (Employee) query.uniqueResult(); // 返回唯一结果
    }
    /*
    条件查询
*/

    @Test
    public void test6(){
//        Criteria criteria = session.createCriteria(Employee.class);// hibernate5 之前的用法
      //  criteria.add(Restrictions.eq("firstName","紫"));
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Employee> criteriaQuery = criteriaBuilder.createQuery(Employee.class);
        criteriaQuery.from(Employee.class);
        Query<Employee> query = session.createQuery(criteriaQuery);
        List<Employee> list = query.list();
        for (Employee employee : list) {
            System.out.println(employee);
        }

    }



}
